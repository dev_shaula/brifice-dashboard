import React from 'react';

const Dashboard = React.lazy(() => import('./pages/Dashboard/Dashboard'));
const Pengajuan = React.lazy(() => import('./pages/Pengajuan/Pengajuan'));
const Nasabah = React.lazy(() => import('./pages/Nasabah/Nasabah'));
const NasabahDetail = React.lazy(() => import('./pages/Nasabah/NasabahDetail'));
const PushNotif = React.lazy(() => import('./pages/Notifikasi/Notifikasi'));
const ProdukInvestasi = React.lazy(() => import('./pages/ProdukInvestasi/Produk'));
const Banner = React.lazy(() => import('./pages/Banner/Banner'));

const routes = [
    { path: '/', exact: true, name: 'Home' },
    { path: '/dashboard', exact: true, name: 'Dashboard', component: Dashboard },
    { path: '/pengajuan', exact: true, name: 'Pengajuan', component: Pengajuan },
    { path: '/nasabah', exact: true, name: 'Nasabah', component: Nasabah },
    { path: '/nasabah/:id', exact: true, name: 'Nasabah Detail', component: NasabahDetail },
    { path: '/push-notifikasi', exact: true, name: 'Push Notifikasi', component: PushNotif },
    { path: '/produk-investasi', exact: true, name: 'Produk Investasi', component: ProdukInvestasi },
    { path: '/banner', exact: true, name: 'Banner', component: Banner }
]

export default routes;