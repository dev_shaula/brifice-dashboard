import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import "./assets/scss/index.scss";
import { Provider } from 'react-redux';
import store from './store';
import { ProtectedRoute } from './middlewares/ProtectedRoute';

const loading = () => <div className="text-center pt-3 animated fadeIn">Loading...</div>

// pages
const Login = React.lazy(() => import('./pages/Login/Login'));

// containers
const DefaultLayout = React.lazy(() => import('./pages/DefaultLayout'));

function App() {
	return (
		<div className="App">
			<Provider store={store}>
				<Router>
					<React.Suspense fallback={loading()}>
						<Switch>
							<Route path="/login" exact name="Login" render={props => <Login {...props} />} />
							<ProtectedRoute path="/" name="Home" component={DefaultLayout} />
						</Switch>
					</React.Suspense>
				</Router>
			</Provider>
		</div>
	);
}

export default App;
