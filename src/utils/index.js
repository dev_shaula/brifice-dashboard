const limit = [
    {
        label: "5",
        value: 5
    },
    {
        label: "10",
        value: 10
    },
    {
        label: "20",
        value: 20
    }
]

const numberFormat = num => {
    return new Intl.NumberFormat("id-ID").format(num)
};

export {
    limit,
    numberFormat
}