const defaultState = {
    loading: false,
    error: '',
    success: false,
    product: []
}

const infoReducers = (state = defaultState, action) => {
    switch (action.type) {
        case "GET_LIST_PRODUCT_FAILED":
            return {
                ...state,
                loading: false,
                error: action.error
            };

        case "GET_LIST_PRODUCT_SUCCESS":
            return {
                ...state,
                loading: false,
                success: action.success,
                product: action.product
            };

        case "GET_LIST_PRODUCT_BEGIN":
            return {
                ...state,
                loading: true,
            }
        default:
            return state;
    }
}

export default infoReducers