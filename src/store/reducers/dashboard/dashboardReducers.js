const defaultState = {
    loading: false,
    error: '',
    success: false,
    dashboard: {
        pengajuan_baru: [],
        total_nasabah: 0,
        total_pengajuan: 0,
        total_pengajuan_baru: 0,
        total_pengajuan_produk: 0
    },
    pengajuanBaru: {
        pengajuan_baru: [],
        total_pengajuan: 0
    },
    totalPengajuan: [],
    pengajuanByProduct: [],
    totalPengguna: []
}

const dashboardReducers = (state = defaultState, action) => {
    switch (action.type) {
        case "GET_DASHBOARD_FAILED":
            return {
                ...state,
                loading: false,
                error: action.error
            };

        case "GET_DASHBOARD_SUCCESS":
            return {
                ...state,
                loading: false,
                success: action.success,
                dashboard: action.dashboard
            };

        case "GET_DASHBOARD_BEGIN":
            return {
                ...state,
                loading: true,
            }

        case "GET_PENGAJUAN_BARU_FAILED":
            return {
                ...state,
                loading: false,
                error: action.error
            };

        case "GET_PENGAJUAN_BARU_SUCCESS":
            return {
                ...state,
                loading: false,
                success: action.success,
                pengajuanBaru: action.pengajuanBaru
            };

        case "GET_PENGAJUAN_BARU_BEGIN":
            return {
                ...state,
                loading: true,
            }

        case "GET_TOTAL_PENGAJUAN_FAILED":
            return {
                ...state,
                loading: false,
                error: action.error
            };

        case "GET_TOTAL_PENGAJUAN_SUCCESS":
            return {
                ...state,
                loading: false,
                success: action.success,
                totalPengajuan: action.totalPengajuan
            };

        case "GET_TOTAL_PENGAJUAN_BEGIN":
            return {
                ...state,
                loading: true,
            }

        case "GET_PENGAJUAN_BY_PRODUCT_FAILED":
            return {
                ...state,
                loading: false,
                error: action.error
            };

        case "GET_PENGAJUAN_BY_PRODUCT_SUCCESS":
            return {
                ...state,
                loading: false,
                success: action.success,
                pengajuanByProduct: action.pengajuanByProduct
            };

        case "GET_PENGAJUAN_BY_PRODUCT_BEGIN":
            return {
                ...state,
                loading: true,
            }

        case "GET_TOTAL_PENGGUNA_FAILED":
            return {
                ...state,
                loading: false,
                error: action.error
            };

        case "GET_TOTAL_PENGGUNA_SUCCESS":
            return {
                ...state,
                loading: false,
                success: action.success,
                totalPengguna: action.totalPengguna
            };

        case "GET_TOTAL_PENGGUNA_BEGIN":
            return {
                ...state,
                loading: true,
            }
        default:
            return state;
    }
}

export default dashboardReducers