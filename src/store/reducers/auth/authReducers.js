const defaultState = {
    loading: false,
    error: '',
    success: false
}

const authReducers = (state = defaultState, action) => {
    switch (action.type) {
        case "AUTH_FAILED":
            return {
                ...state,
                loading: false,
                error: action.error
            };

        case "AUTH_SUCCESS":
            return {
                ...state,
                loading: false,
                success: action.success
            };

        case "AUTH_BEGIN":
            return {
                ...state,
                loading: true,
            }
        default:
            return state;
    }
}

export default authReducers