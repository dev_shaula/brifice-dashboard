const defaultState = {
    loading: false,
    error: '',
    success: false,
    pengajuan: []
}

const pengajuanReducers = (state = defaultState, action) => {
    switch (action.type) {
        case "GET_PENGAJUAN_FAILED":
            return {
                ...state,
                loading: false,
                error: action.error
            };

        case "GET_PENGAJUAN_SUCCESS":
            return {
                ...state,
                loading: false,
                success: action.success,
                pengajuan: action.pengajuan
            };

        case "GET_PENGAJUAN_BEGIN":
            return {
                ...state,
                loading: true,
            }
        default:
            return state;
    }
}

export default pengajuanReducers