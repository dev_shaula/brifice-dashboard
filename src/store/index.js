import authReducers from "./reducers/auth/authReducers";
import { combineReducers, createStore, applyMiddleware } from "redux";
import ReduxThunk from "redux-thunk";
import dashboardReducers from "./reducers/dashboard/dashboardReducers";
import infoReducers from "./reducers/info/infoReducers";
import pengajuanReducers from "./reducers/pengajuan/pengajuanReducers";

const rootReducers = combineReducers({
    auth: authReducers,
    dashboard: dashboardReducers,
    info: infoReducers,
    pengajuan: pengajuanReducers
})

const store = createStore(rootReducers, applyMiddleware(ReduxThunk))

export default store