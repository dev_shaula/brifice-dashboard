import axios from "axios";

const getDashboard = () => {
    return async dispatch => {
        dispatch({
            type: "GET_DASHBOARD_BEGIN",
            loading: true
        })
        try {
            await axios.get(`${process.env.REACT_APP_DEV_API}web/`, {
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem("token")}`,
                    'Content-Type': 'application/json',
                    'Accept': '*/*',
                },
            })
                .then(res => {
                    if (res.data.status !== "success") {
                        dispatch({
                            type: "GET_DASHBOARD_FAILED",
                            success: false,
                            error: res.data.status
                        })
                    }
                    dispatch({
                        type: "GET_DASHBOARD_SUCCESS",
                        success: true,
                        dashboard: res.data.data
                    })
                })
                .catch(err => {
                    dispatch({
                        type: "GET_DASHBOARD_FAILED",
                        success: false,
                        error: err.response.statusText
                    })
                    console.log(err)
                })
        } catch (error) {
            dispatch({
                type: "GET_DASHBOARD_FAILED",
                success: false,
                error: error.response
            })
            console.log(error.message)
        }
    }
}

export default getDashboard