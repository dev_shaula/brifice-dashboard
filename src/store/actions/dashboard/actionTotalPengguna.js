import axios from "axios";

const getTotalPengguna = (cb) => {
    return async dispatch => {
        dispatch({
            type: "GET_TOTAL_PENGGUNA_BEGIN",
            loading: true
        })
        try {
            await axios.get(`${process.env.REACT_APP_DEV_API}web/dashboard/nasabah`, {
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem("token")}`,
                    'Content-Type': 'application/json',
                    'Accept': '*/*',
                },
            })
                .then(res => {
                    if (res.data.status !== "success") {
                        dispatch({
                            type: "GET_TOTAL_PENGGUNA_FAILED",
                            success: false,
                            error: res.data.status
                        })
                    }

                    const totalNasabah = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

                    for (let i = 0; i < res.data.data.graf.length; i++) {
                        totalNasabah.splice(res.data.data.graf[i].bulan - 1, 1, res.data.data.graf[i].jumlah)
                    }

                    dispatch({
                        type: "GET_TOTAL_PENGGUNA_SUCCESS",
                        success: true,
                        totalPengguna: totalNasabah
                    })
                    cb(res.data.data.graf)
                })
                .catch(err => {
                    dispatch({
                        type: "GET_TOTAL_PENGGUNA_FAILED",
                        success: false,
                        error: err.response.statusText
                    })
                    console.log(err)
                })
        } catch (error) {
            dispatch({
                type: "GET_TOTAL_PENGGUNA_FAILED",
                success: false,
                error: error.response
            })
            console.log(error.message)
        }
    }
}

export default getTotalPengguna