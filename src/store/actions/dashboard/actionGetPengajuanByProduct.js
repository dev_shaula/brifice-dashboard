import axios from "axios";

const getPengajuanByProduct = (id, cb) => {
    return async dispatch => {
        dispatch({
            type: "GET_PENGAJUAN_BY_PRODUCT_BEGIN",
            loading: true
        })
        try {
            await axios.get(`${process.env.REACT_APP_DEV_API}web/dashboard/produk?produk=${id}`, {
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem("token")}`,
                    'Content-Type': 'application/json',
                    'Accept': '*/*',
                },
            })
                .then(res => {
                    if (res.data.status !== "success") {
                        dispatch({
                            type: "GET_PENGAJUAN_BY_PRODUCT_FAILED",
                            success: false,
                            error: res.data.status
                        })
                    }

                    const total = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

                    for (let index = 0; index < res.data.data.graf.length; index++) {
                        total.splice(res.data.data.graf[index].bulan - 1, 1, res.data.data.graf[index].jumlah)
                    }

                    dispatch({
                        type: "GET_PENGAJUAN_BY_PRODUCT_SUCCESS",
                        success: true,
                        pengajuanByProduct: total
                    })
                    cb(res.data.data.graf)
                })
                .catch(err => {
                    dispatch({
                        type: "GET_PENGAJUAN_BY_PRODUCT_FAILED",
                        success: false,
                        error: err.response.statusText
                    })
                    console.log(err)
                })
        } catch (error) {
            dispatch({
                type: "GET_PENGAJUAN_BY_PRODUCT_FAILED",
                success: false,
                error: error.response
            })
            console.log(error.message)
        }
    }
}

export default getPengajuanByProduct