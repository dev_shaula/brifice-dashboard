import axios from "axios";

const getPengajuanBaru = () => {
    return async dispatch => {
        dispatch({
            type: "GET_PENGAJUAN_BARU_BEGIN",
            loading: true
        })
        try {
            await axios.get(`${process.env.REACT_APP_DEV_API}web/dashboard/baru`, {
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem("token")}`,
                    'Content-Type': 'application/json',
                    'Accept': '*/*',
                },
            })
                .then(res => {
                    if (res.data.status !== "success") {
                        dispatch({
                            type: "GET_PENGAJUAN_BARU_FAILED",
                            success: false,
                            error: res.data.status
                        })
                    }
                    dispatch({
                        type: "GET_PENGAJUAN_BARU_SUCCESS",
                        success: true,
                        pengajuanBaru: res.data.data
                    })
                })
                .catch(err => {
                    dispatch({
                        type: "GET_PENGAJUAN_BARU_FAILED",
                        success: false,
                        error: err.response.statusText
                    })
                    console.log(err)
                })
        } catch (error) {
            dispatch({
                type: "GET_PENGAJUAN_BARU_FAILED",
                success: false,
                error: error.response
            })
            console.log(error.message)
        }
    }
}

export default getPengajuanBaru