import axios from "axios";

const getListProduct = () => {
    return async dispatch => {
        dispatch({
            type: "GET_LIST_PRODUCT_BEGIN",
            loading: true
        })
        try {
            await axios.get(`${process.env.REACT_APP_DEV_API}produk/`, {
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem("token")}`,
                    'Content-Type': 'application/json',
                    'Accept': '*/*',
                },
            })
                .then(res => {
                    if (res.data.status !== "success") {
                        dispatch({
                            type: "GET_LIST_PRODUCT_FAILED",
                            success: false,
                            error: res.data.status
                        })
                    }
                    dispatch({
                        type: "GET_LIST_PRODUCT_SUCCESS",
                        success: true,
                        product: res.data.data
                    })
                })
                .catch(err => {
                    dispatch({
                        type: "GET_LIST_PRODUCT_FAILED",
                        success: false,
                        error: err.response.statusText
                    })
                    console.log(err)
                })
        } catch (error) {
            dispatch({
                type: "GET_LIST_PRODUCT_FAILED",
                success: false,
                error: error.response
            })
            console.log(error.message)
        }
    }
}

export default getListProduct