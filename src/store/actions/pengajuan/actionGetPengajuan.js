import axios from "axios";

const getPengajuan = (params) => {
    return async dispatch => {
        dispatch({
            type: "GET_PENGAJUAN_BEGIN",
            loading: true
        })
        try {
            await axios.get(`${process.env.REACT_APP_DEV_API}web/pengajuan?page=${params.page}&limit=${params.limit}`, {
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem("token")}`,
                    'Content-Type': 'application/json',
                    'Accept': '*/*',
                },
            })
                .then(res => {
                    if (res.data.status !== "success") {
                        dispatch({
                            type: "GET_PENGAJUAN_FAILED",
                            success: false,
                            error: res.data.status
                        })
                    }
                    dispatch({
                        type: "GET_PENGAJUAN_SUCCESS",
                        success: true,
                        pengajuan: res.data.data ? res.data.data : []
                    })
                })
                .catch(err => {
                    dispatch({
                        type: "GET_PENGAJUAN_FAILED",
                        success: false,
                        error: err.response.statusText
                    })
                    console.log(err)
                })
        } catch (error) {
            dispatch({
                type: "GET_PENGAJUAN_FAILED",
                success: false,
                error: error.response
            })
            console.log(error.message)
        }
    }
}

export default getPengajuan