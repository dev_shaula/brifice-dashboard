const isAuthenticated = () => {
    const token = (localStorage.getItem("token")) ? localStorage.getItem("token") : null
    const hasLogin = (localStorage.getItem("hasLogin")) ? localStorage.getItem("hasLogin") : null

    if (token && hasLogin) {
        return true;
    }

    localStorage.removeItem("token");
    localStorage.removeItem("hasLogin");
    return false;
}

export default isAuthenticated