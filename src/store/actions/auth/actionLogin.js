import axios from "axios";

const login = (data, cb) => {
    return async dispatch => {
        dispatch({
            type: "AUTH_BEGIN",
            loading: true
        })
        try {
            await axios.post(`${process.env.REACT_APP_DEV_API}admin/auth`, data, {
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "*/*"
                }
            })
            .then(res => {
                console.log(res.data)
                if (res.data.status !== "success") {
                    dispatch({
                        type: "AUTH_FAILED",
                        success: false,
                        error: res.data.status
                    })
                }
                localStorage.setItem("hasLogin", true)
                localStorage.setItem("token", res.data.data.token)
                dispatch({
                    type: "AUTH_SUCCESS",
                    success: true
                })
                cb(true, null);
            })
            .catch(err => {
                dispatch({
                    type: "AUTH_FAILED",
                    success: false,
                    error: err.response.statusText
                })
                console.log(err.response)
            })
        } catch (error) {
            dispatch({
                type: "AUTH_FAILED",
                success: false,
                error: error.response
            })
        }
    }
}

export default login