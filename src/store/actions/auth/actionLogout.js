const logout = () => {
    console.log('logout')
    localStorage.removeItem("hasLogin")
    localStorage.removeItem("token")
}

export default logout