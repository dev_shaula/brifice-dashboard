import React from 'react';
import propTypes from 'prop-types';
import './Component.scss';

const Table = ({ thead, renderRow, data, emptyMessage }) => {
    return (
        <table className="table table-striped table-bordered">
            <thead>
                <tr>
                    {
                        thead.map((val, idx) => (
                            <th key={idx} scope="col">{val}</th>
                        ))
                    }
                </tr>
            </thead>
            <tbody>
                {
                    data.length > 0 ? (
                        data.map((val, idx) => (
                            renderRow(val, idx)
                        ))
                    ) : (
                        <tr>
                            <td colSpan="8" style={{ textAlign: "center" }}>{emptyMessage ? emptyMessage : "Tidak ada data untuk saat ini"}</td>
                        </tr>
                    )
                }
            </tbody>
        </table>
    )
}

export default Table

Table.propTypes = {
    thead: propTypes.array,
    renderRow: propTypes.func,
    data: propTypes.array,
    emptyMessage: propTypes.string
}