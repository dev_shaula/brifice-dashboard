import React from 'react';
import { AiFillBell } from 'react-icons/ai';
import { Link } from 'react-router-dom';
import { IconLogo, Profile } from '../../assets';
import './Component.scss';

const Header = () => {
    return (
        <nav className="navbar navbar-expand-lg navbar-white bg-white">
            <div className="container">
                <div className="logo">
                    <img src={IconLogo} alt="logo" className="img-login" />
                </div>

                <div className="collapse navbar-collapse justify-content-end">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <Link to="/notification">
                                <AiFillBell size={20} color="#D0D0D0" />
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/dashboard">
                                <img src={Profile} alt="profile" className="profile-img" />
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    )
}

export default Header
