import React from 'react';
import propTypes from 'prop-types';
import "./Component.scss";

const Component = (props) => {
    const className = ["btn", props.className]
    props.isDisabled && className.push("disabled")

    return (
        <button
            type="submit"
            onClick={props.onClick}
            className={className.join(" ")}
        >
            {props.children}
        </button>
    )
}

export default Component

Component.propTypes = {
    onClick: propTypes.func,
    className: propTypes.string,
    isDisabled: propTypes.bool
}