import React from 'react';
import propTypes from 'prop-types';
import './Component.scss';
import { NavLink } from 'react-router-dom';

const Menu = ({ icon, iconActive, title, checkActive, router, onClick }) => {
    return (
        <div className={checkActive ? "menu-item-active" : "menu-item"}>
            <div className="item">
                <div className="icon">
                    {
                        checkActive ? (
                            iconActive
                        ) : (
                            icon
                        )
                    }
                </div>
                <NavLink to={`${router === "/logout" ? "/login" : router}`} style={{fontWeight: 500, fontSize: "16px"}} onClick={() => {
                    if (router === "/logout") {
                        localStorage.removeItem("token")
                        localStorage.removeItem("hasLogin")
                    } 
                }}>
                    {title}
                </NavLink>
            </div>
        </div>
    )
}

export default Menu

Menu.propTypes = {
    icon: propTypes.node,
    iconActive: propTypes.node,
    title: propTypes.string,
    checkActive: propTypes.bool,
    router: propTypes.string,
    onClick: propTypes.func
}