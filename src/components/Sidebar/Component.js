import React from 'react';
import { MdDashboard, MdArchive } from 'react-icons/md';
import { AiFillBell } from 'react-icons/ai';
import { IoIosPeople } from 'react-icons/io';
import { IoMegaphoneOutline, IoLogOutSharp } from 'react-icons/io5';
import { FaPiggyBank } from 'react-icons/fa';
import './Component.scss';
import Menu from './Menu';
import { useHistory, useLocation } from 'react-router-dom';

const Sidebar = () => {
    const history = useHistory()
    const { pathname } = useLocation();
    const checkActive = (path) => {
        return pathname.includes(path.toLowerCase())
    }

    return (
        <div className="sidebar">
            <div className="menu">
                <Menu
                    title="Dashboard"
                    router="/dashboard"
                    iconActive={<MdDashboard color="#5683EC" size={20} onClick={() => history.push("/dashboard")} />}
                    icon={<MdDashboard color="#C4C4C4" size={20} onClick={() => history.push("/dashboard")} />}
                    checkActive={checkActive('dashboard')}
                />
                <Menu
                    title="Pengajuan"
                    router="/pengajuan"
                    iconActive={<MdArchive color="#5683EC" size={20} onClick={() => history.push("/dashboard")} />}
                    icon={<MdArchive color="#C4C4C4" size={20} onClick={() => history.push("/dashboard")} />}
                    checkActive={checkActive('pengajuan')}
                />
                <Menu
                    title="Nasabah"
                    router="/nasabah"
                    iconActive={<IoIosPeople color="#5683EC" size={20} onClick={() => history.push("/dashboard")} />}
                    icon={<IoIosPeople color="#C4C4C4" size={20} onClick={() => history.push("/dashboard")} />}
                    checkActive={checkActive('nasabah')}
                />
                <Menu
                    title="Banner"
                    router="/banner"
                    iconActive={<IoMegaphoneOutline color="#5683EC" size={20} onClick={() => history.push("/banner")} />}
                    icon={<IoMegaphoneOutline color="#C4C4C4" size={20} onClick={() => history.push("/banner")} />}
                    checkActive={checkActive('banner')}
                />
                <Menu
                    title="Push Notifikasi"
                    router="/push-notifikasi"
                    iconActive={<AiFillBell color="#5683EC" size={20} onClick={() => history.push("/dashboard")} />}
                    icon={<AiFillBell color="#C4C4C4" size={20} onClick={() => history.push("/dashboard")} />}
                    checkActive={checkActive('push-notifikasi')}
                />
                <Menu
                    title="Produk Investasi"
                    router="/produk-investasi"
                    iconActive={<FaPiggyBank color="#5683EC" size={20} onClick={() => history.push("/dashboard")} />}
                    icon={<FaPiggyBank color="#C4C4C4" size={20} onClick={() => history.push("/dashboard")} />}
                    checkActive={checkActive('produk-investasi')}
                />
                <div className="menu-bottom">
                    <Menu
                        title="Logout"
                        router="/logout"
                        icon={<IoLogOutSharp color="#C4C4C4" size={20} />}
                    />
                </div>
            </div>
        </div>
    )
}

export default Sidebar
