import React, { useState } from 'react';
import propTypes from 'prop-types';
import { FaUser } from "react-icons/fa";
import { IoKeyOutline } from 'react-icons/io5';
import { BsEyeSlash, BsEye } from 'react-icons/bs';
import "./Component.scss";

const Component = (props) => {
    const [inputValue, setInputValue] = useState(`${props.value}`);
    const [show, setShow] = useState(false);

    const handleChangeInput = e => {
        props.onChange({
            target: {
                name: props.name,
                value: e.target.value
            }
        });
        setInputValue(e.target.value);
    }

    return (
        <div className="form-group">
            <div className="icon">
                {
                    props.isPassword ? (
                        <IoKeyOutline size={20} color="#8E8E8E" />
                    ) : (
                        <FaUser size={20} color="#8E8E8E" />
                    )
                }
            </div>
            <input
                type={`${props.isPassword ? show ? "text" : "password" : "text"}`}
                name={props.name}
                placeholder={props.placeholder}
                onChange={handleChangeInput}
                className={["form-control", props.className].join(" ")}
                value={inputValue}
            />
            <div className="icon-password">
                {
                    props.isPassword ? (
                        show ? (
                            <BsEye color="#8E8E8E" size={20} onClick={() => setShow(false)} />
                        ) : (
                            <BsEyeSlash color="#8E8E8E" size={20} onClick={() => setShow(true)} />
                        )
                    ) : (
                        null
                    )
                }
            </div>
        </div>
    )
}

export default Component

Component.propTypes = {
    onChange: propTypes.func,
    value: propTypes.string,
    className: propTypes.string,
    placeholder: propTypes.string,
    name: propTypes.string,
    isPassword: propTypes.bool
}