import React from 'react';
import { IconClose } from '../../assets';
import './Component.scss';

const Component = ({ onClose, body }) => {
    return (
        <div className="modal-container">
            <div className="modal-background"></div>
            <div className="modal-body">
                <div className="close-modal" onClick={onClose}>
                    <img src={IconClose} alt="" />
                </div>
                {body}
            </div>
        </div>
    )
}

export default Component
