import React from 'react'
import { Redirect, Route } from 'react-router-dom'

export const ProtectedRoute = ({ component: Component, ...rest }) => {
    const isAuthenticated = () => {
        const token = (localStorage.getItem("token")) ? localStorage.getItem("token") : null
        const hasLogin = (localStorage.getItem("hasLogin")) ? localStorage.getItem("hasLogin") : null

        if (token && hasLogin) {
            return true
        }

        localStorage.removeItem("token")
        localStorage.removeItem("hasLogin")
        return false
    }
    console.log(isAuthenticated())
    return (
        <Route
            {...rest}
            render={props => {
                if (isAuthenticated()) {
                    return <Component {...props} />;
                } else {
                    return (<Redirect to={{ pathname: "/login", state: { from: props.location } }} />);
                }
            }}
        />
    )
}
