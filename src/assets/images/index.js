import LoginLogo from './login-logo.png';
import BRILogo from './BRILogo.png';
import Profile from './profile.png';
import LoginBg from './login-bg.png';
import ExampleBanner from './example-banner.png';

export {
    LoginLogo,
    BRILogo,
    Profile,
    LoginBg,
    ExampleBanner
}