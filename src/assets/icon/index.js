import IconLogo from './ic_logo.svg';
import IconClose from './ic_close.svg';
import IconUploadBanner from './ic_upload_banner.svg';

export {
    IconLogo,
    IconClose,
    IconUploadBanner
}