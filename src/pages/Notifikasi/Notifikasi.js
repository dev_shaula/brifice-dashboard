import { Button } from '../../components';
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import './Notifikasi.scss';

const Notifikasi = () => {
    return (
        <div className="notifikasi">
            <h6>Push Notifikasi</h6>
            <Form style={{ marginTop: "30px" }}>
                <FormGroup>
                    <Label for="title" style={{ paddingBottom: "10px" }}>Title Notifikasi</Label>
                    <Input style={{ paddingLeft: "10px" }} type="text" name="title" id="title" placeholder="Contoh : Pemeliharaan Sistem" />
                </FormGroup>
                <FormGroup>
                    <Label for="deskripsi" style={{ paddingBottom: "10px" }}>Deskripsi Singkat</Label>
                    <Input style={{ paddingLeft: "10px" }} type="text" name="deskripsi" id="deskripsi" placeholder="Contoh : Akan ada pemeliharaan sistem hari ini pukul 21:00 - 23:59 WIB" />
                </FormGroup>
                <FormGroup>
                    <Label for="link" style={{ paddingBottom: "10px" }}>Link</Label>
                    <Input style={{ paddingLeft: "10px" }} type="text" name="link" id="link" placeholder="Contoh : -" />
                </FormGroup>
                <Button className="mt-2">
                    <h6 className="bold text-white">PUSH</h6>
                </Button>
            </Form>
        </div>
    )
}

export default Notifikasi
