import React, { Suspense } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { Header, Sidebar } from '../../components';
import routes from '../../routes';

const DefaultLayout = () => {
    const loading = () => <div className="text-center pt-3 animated fadeIn">Loading...</div>
    return (
        <div>
            <Suspense fallback={loading()}>
                <Header />
            </Suspense>
            <div className="wrapper">
                <div className="wrapper-content container">
                    <Sidebar />
                    <div className="content">
                        <Suspense fallback={loading()}>
                            <Switch>
                                {routes.map((route, idx) => {
                                    return route.component ? (
                                        <Route
                                            key={idx}
                                            path={route.path}
                                            exact={route.exact}
                                            name={route.name}
                                            render={props => (
                                                <route.component {...props} />
                                            )}
                                        />
                                    ) : (null)
                                })}
                                <Redirect from="/" to="/dashboard" />
                            </Switch>
                        </Suspense>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default DefaultLayout
