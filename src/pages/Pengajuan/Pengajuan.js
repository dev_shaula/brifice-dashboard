import React, { useEffect, useState } from 'react';
import { Input } from 'reactstrap';
import { Pagination, SelectPicker } from 'rsuite';
import { FiSearch } from 'react-icons/fi';
import { Table } from '../../components';
import PengajuanRow from './PengajuanRow';
import './Pengajuan.scss';
import { limit } from '../../utils';
import { useDispatch, useSelector } from 'react-redux';
import { actionGetPengajuan } from '../../store/actions';

const Pengajuan = () => {
    const { pengajuan } = useSelector(state => state.pengajuan);
    const [page, setPage] = useState(1);
    const [limitData, setLimitData] = useState(10);
    const dispatch = useDispatch();

    const [state, setState] = useState({
        prev: true,
        next: true,
        ellipsis: true,
        boundaryLinks: true,
        activePage: 1
    })

    console.log(state)

    const handleSelect = (eventKey) => {
        setState({ activePage: eventKey });
        setPage(eventKey)
        dispatch(actionGetPengajuan({ page: eventKey, limit: limitData }))
    }

    useEffect(() => {
        dispatch(actionGetPengajuan({ page, limit: limitData }))
    }, [dispatch, limitData, page])

    return (
        <div className="pengajuan">
            <h6 style={{ marginBottom: "25px" }}>Pengajuan</h6>
            <div style={{ display: "flex", justifyContent: "space-between", marginBottom: "30px" }}>
                <SelectPicker searchable={false} data={limit} style={{ width: 80 }} value={10} onChange={(e) => setLimitData(e)} />
                <div style={{ display: "flex", position: "relative" }}>
                    <div style={{ position: "absolute", top: "9px", left: "15px", paddingRight: "25px" }}>
                        <FiSearch size={18} color="#4A4A4A" />
                    </div>
                    <Input
                        type="text"
                        name="search"
                        placeholder="Pencarian..."
                        style={{
                            width: 350,
                            height: 40,
                            borderRadius: "10px",
                            boxShadow: "none",
                            paddingLeft: "50px"
                        }}
                    />
                </div>
            </div>
            <Table
                thead={[
                    "No",
                    "Time Stamp",
                    "Nama",
                    "Produk Investasi",
                    "Jumlah",
                    "Status"
                ]}
                data={pengajuan}
                renderRow={(item, key) => (
                    <PengajuanRow
                        item={item}
                        key={key}
                        idx={key}
                    />
                )}
            />
            <div style={{ display: "flex", justifyContent: "space-between", marginTop: "40px" }}>
                <Pagination
                    prev
                    last
                    next
                    first
                    pages={pengajuan.length}
                    maxButtons={5}
                    activePage={state.activePage}
                    onSelect={handleSelect}
                    size="sm"
                />
                <span style={{ color: "#898A8D" }}>Showing <b>{page} - {pengajuan.length}</b> of 1000 Items</span>
            </div>
        </div>
    )
}

export default Pengajuan
