import React from 'react';
import propTypes from 'prop-types';
import './Pengajuan.scss';
import moment from 'moment';
import { numberFormat } from '../../utils';

const PengajuanRow = ({ item, idx }) => {
    return (
        <tr key={item.no}>
            <td>{idx + 1}</td>
            <td>{moment(item.timestamp).format("DD MMM YYYY ; HH:MM")}</td>
            <td>{item.nama}</td>
            <td>{item.produk_investasi}</td>
            <td>Rp {numberFormat(item.jumlah_pengajuan)}</td>
            <td>
                <div className={`status ${item.status === "submission" ? "status-subs" : item.status === "Forwarded" ? "status-forward" : item.status === "Approve" ? "status-approve" : "status-reject"}`}>
                    {item.status}
                </div>
            </td>
        </tr>
    )
}

export default PengajuanRow

PengajuanRow.propTypes = {
    item: propTypes.object
}