import React from 'react';
import propTypes from 'prop-types';
import './Nasabah.scss';

const NasabahRow = ({ item, detailNasabah }) => {
    return (
        <tr key={item.no} onClick={detailNasabah}>
            <td>{item.no}</td>
            <td>{item.register}</td>
            <td>{item.nama}</td>
            <td>{item.username}</td>
            <td>{item.Jumlah}</td>
        </tr>
    )
}

export default NasabahRow

NasabahRow.propTypes = {
    item: propTypes.object,
    detailNasabah: propTypes.func
}