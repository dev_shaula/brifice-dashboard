import React, { useState } from 'react';
import { Input } from 'reactstrap';
import { Pagination, SelectPicker } from 'rsuite';
import { FiSearch } from 'react-icons/fi';
import { Table } from '../../components';
import './Nasabah.scss';
import NasabahRow from './NasabahRow';
import { useHistory } from 'react-router-dom';
import { limit } from '../../utils';

const Nasabah = () => {
    const history = useHistory()
    const pengajuan = [
        {
            no: 1,
            register: "12 Jul 2021 ; 12:40",
            nama: "Kadin Press",
            username: "Davestera",
            Jumlah: "Rp 5.000.000",
        },
        {
            no: 2,
            register: "12 Jul 2021 ; 12:40",
            nama: "Kadin Press",
            username: "Davestera",
            Jumlah: "Rp 5.000.000"
        },
        {
            no: 3,
            register: "12 Jul 2021 ; 12:40",
            nama: "Kadin Press",
            username: "Davestera",
            Jumlah: "Rp 5.000.000"
        },
        {
            no: 4,
            register: "12 Jul 2021 ; 12:40",
            nama: "Kadin Press",
            username: "Davestera",
            Jumlah: "Rp 5.000.000"
        },
        {
            no: 5,
            register: "12 Jul 2021 ; 12:40",
            nama: "Kadin Press",
            username: "Davestera",
            Jumlah: "Rp 5.000.000"
        }
    ]

    const [state, setState] = useState({
        prev: true,
        next: true,
        ellipsis: true,
        boundaryLinks: true,
        activePage: 1
    })

    const handleSelect = (eventKey) => {
        setState({ activePage: eventKey });
    }

    return (
        <div>
            <h6 style={{ marginBottom: "25px" }}>Pengajuan</h6>
            <div style={{ display: "flex", justifyContent: "space-between", marginBottom: "30px" }}>
                <SelectPicker searchable={false} data={limit} style={{ width: 80 }} value={10} />
                <div style={{ display: "flex", position: "relative" }}>
                    <div style={{ position: "absolute", top: "9px", left: "15px", paddingRight: "25px" }}>
                        <FiSearch size={18} color="#4A4A4A" />
                    </div>
                    <Input
                        type="text"
                        name="search"
                        placeholder="Pencarian..."
                        style={{
                            width: 350,
                            height: 40,
                            borderRadius: "10px",
                            boxShadow: "none",
                            paddingLeft: "50px"
                        }}
                    />
                </div>
            </div>
            <Table
                thead={[
                    "No",
                    "Register",
                    "Nama",
                    "Username",
                    "Jumlah Investasi"
                ]}
                data={pengajuan}
                renderRow={(item, key) => (
                    <NasabahRow
                        item={item}
                        detailNasabah={() => history.push({ pathname: `/nasabah/${item.no}`, state: { detail: item } })}
                        key={key}
                    />
                )}
            />
            <div style={{ display: "flex", justifyContent: "space-between", marginTop: "40px" }}>
                <Pagination
                    prev
                    last
                    next
                    first
                    pages={30}
                    maxButtons={5}
                    activePage={state.activePage}
                    onSelect={handleSelect}
                    size="sm"
                />
                <span style={{ color: "#898A8D" }}>Showing <b>1 - 10</b> of 1000 Items</span>
            </div>
        </div>
    )
}

export default Nasabah
