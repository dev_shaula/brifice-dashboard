import React from 'react';
import propTypes from 'prop-types';
import '../Nasabah.scss';

const PortofolioRow = ({item}) => {
    return (
        <tr key={item.no}>
            <td>{item.no}</td>
            <td>{item.pengajuan}</td>
            <td>{item.nama}</td>
            <td>{item.jumlah}</td>
            <td>{item.pendapatan}</td>
        </tr>
    )
}

export default PortofolioRow

PortofolioRow.propTypes = {
    item: propTypes.object
}