import React from 'react';
import { Doughnut } from 'react-chartjs-2';
import '../Nasabah.scss';

const Pemasukan = () => {
    return (
        <div className="section-pemasukan">
            <h1>Analisis Pemasukan</h1>
            <div className="rata-rata">
                <p>Rata Rata pemasukan</p>
                <h1 style={{ fontWeight: "bold", fontSize: "18px" }}>Rp 15.000.000</h1>
            </div>
            <div className="doughnut-chart">
                <Doughnut
                    options={{
                        legend: {
                            display: false,
                        },
                        cutoutPercentage: 60,
                        tooltips: {
                            callbacks: {
                                label: (tooltipItem, data) => {
                                    let label =
                                        (data.datasets[tooltipItem.datasetIndex].labels &&
                                            data.datasets[tooltipItem.datasetIndex].labels[
                                            tooltipItem.index
                                            ]) ||
                                        data.labels[tooltipItem.index] ||
                                        "";
                                    if (label) {
                                        label += ": ";
                                    }

                                    // Apply the value and suffix
                                    label +=
                                        data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] +
                                        (data.datasets[tooltipItem.datasetIndex].labelSuffix || "");

                                    return label;
                                }
                            }
                        }
                    }}
                    data={{
                        datasets: [{
                            data: [50, 50],
                            backgroundColor: [
                                '#2736C1',
                                '#FF9D07'
                            ],
                            hoverBackgroundColor: [
                                '#2736C1',
                                '#FF9D07'
                            ]
                        }]
                    }}
                    height={200}
                    width={200}
                />
            </div>
            <div className="salary-credit">
                <div className="box-salary"></div>
                <p>Salary Crediting</p>
            </div>
            <div className="auto-payroll">
                <div className="box-auto"></div>
                <p>Auto Payroll Embrio BRI</p>
            </div>
        </div>
    )
}

export default Pemasukan
