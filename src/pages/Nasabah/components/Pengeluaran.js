import React from 'react';
import { Doughnut } from 'react-chartjs-2';
import '../Nasabah.scss';

const Pengeluaran = () => {
    return (
        <div className="section-pengeluaran">
            <h1>Analisis Pengeluaran</h1>
            <div className="rata-rata">
                <p>Rata rata Pengeluaran</p>
                <h1>Rp 15.000.000</h1>
            </div>
            <div className="doughnut-chart">
                <Doughnut
                    options={{
                        legend: {
                            display: false,
                        },
                        cutoutPercentage: 60,
                        tooltips: {
                            callbacks: {
                                label: (tooltipItem, data) => {
                                    let label =
                                        (data.datasets[tooltipItem.datasetIndex].labels &&
                                            data.datasets[tooltipItem.datasetIndex].labels[
                                            tooltipItem.index
                                            ]) ||
                                        data.labels[tooltipItem.index] ||
                                        "";
                                    if (label) {
                                        label += ": ";
                                    }

                                    // Apply the value and suffix
                                    label +=
                                        data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] +
                                        (data.datasets[tooltipItem.datasetIndex].labelSuffix || "");

                                    return label;
                                }
                            }
                        }
                    }}
                    data={{
                        datasets: [{
                            data: [50, 50],
                            backgroundColor: [
                                '#2736C1',
                                '#FF9D07'
                            ],
                            hoverBackgroundColor: [
                                '#2736C1',
                                '#FF9D07'
                            ]
                        }]
                    }}
                    height={200}
                    width={200}
                />
            </div>
            <div className="kost">
                <div className="box-kost"></div>
                <p>Transfer to KOST</p>
            </div>
            <div className="hallo">
                <div className="box-hallo"></div>
                <p>Hallo Pascabayar</p>
            </div>
            <div className="pln">
                <div className="box-pln"></div>
                <p>PLN Prepaid</p>
            </div>
            <div className="netflix">
                <div className="box-netflix"></div>
                <p>Bill Payment - Netflix</p>
            </div>
            <div className="atm">
                <div className="box-atm"></div>
                <p>Withdrawal - ATM</p>
            </div>
            <div className="gopay">
                <div className="box-gopay"></div>
                <p>Topup Gopay</p>
            </div>
        </div>
    )
}

export default Pengeluaran
