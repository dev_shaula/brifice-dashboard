import React, { useState } from 'react';
import { Dropdown, Pagination } from 'rsuite';
import { Table } from '../../../components';
import '../Nasabah.scss';
import PortofolioRow from './PortofolioRow';

const Portofolio = () => {
    const SizeDropdown = props => (
        <Dropdown appearance="default" {...props}>
            <Dropdown.Item>New File</Dropdown.Item>
            <Dropdown.Item>New File with Current Profile</Dropdown.Item>
            <Dropdown.Item>Download As...</Dropdown.Item>
            <Dropdown.Item>Export PDF</Dropdown.Item>
            <Dropdown.Item>Export HTML</Dropdown.Item>
            <Dropdown.Item>Settings</Dropdown.Item>
            <Dropdown.Item>About</Dropdown.Item>
        </Dropdown>
    )

    const pengajuan = [
        {
            no: 1,
            pengajuan: "12 Jul 2021 ; 12:40",
            nama: "Davestera",
            jumlah: "Rp 5.000.000",
            pendapatan: "Rp 5.000.000",
        },
        {
            no: 2,
            pengajuan: "12 Jul 2021 ; 12:40",
            nama: "Davestera",
            jumlah: "Rp 5.000.000",
            pendapatan: "Rp 5.000.000",
        },
        {
            no: 3,
            pengajuan: "12 Jul 2021 ; 12:40",
            nama: "Davestera",
            jumlah: "Rp 5.000.000",
            pendapatan: "Rp 5.000.000",
        },
        {
            no: 4,
            pengajuan: "12 Jul 2021 ; 12:40",
            nama: "Davestera",
            jumlah: "Rp 5.000.000",
            pendapatan: "Rp 5.000.000",
        },
        {
            no: 5,
            pengajuan: "12 Jul 2021 ; 12:40",
            nama: "Davestera",
            jumlah: "Rp 5.000.000",
            pendapatan: "Rp 5.000.000",
        }
    ]

    const [state, setState] = useState({
        prev: true,
        next: true,
        ellipsis: true,
        boundaryLinks: true,
        activePage: 1
    })

    const handleSelect = (eventKey) => {
        setState({ activePage: eventKey });
    }
    return (
        <div>
            <div style={{display: "flex", justifyContent: "space-between"}}>
                <div className="total-investasi">
                    <p>Total Investasi</p>
                    <h1><b>Rp. 55.000.000</b></h1>
                </div>
                <div className="total-pendapatan">
                    <p>Pendapatan</p>
                    <h1><b>Rp. 5.900.000</b></h1>
                </div>
            </div>
            <div style={{ marginTop: "30px" }}>
                <SizeDropdown title="10" size="lg" />
                <Table
                    thead={[
                        "No",
                        "Pengajuan",
                        "Nama",
                        "Jumlah Investasi",
                        "Pendapatan"
                    ]}
                    data={pengajuan}
                    renderRow={(item) => (
                        <PortofolioRow
                            item={item}
                        />
                    )}
                />
                <div style={{ display: "flex", justifyContent: "space-between", marginTop: "40px" }}>
                    <Pagination
                        prev
                        last
                        next
                        first
                        pages={30}
                        maxButtons={5}
                        activePage={state.activePage}
                        onSelect={handleSelect}
                        size="sm"
                    />
                    <span style={{ color: "#898A8D" }}>Showing <b>1 - 10</b> of 1000 Items</span>
                </div>
            </div>
        </div>
    )
}

export default Portofolio
