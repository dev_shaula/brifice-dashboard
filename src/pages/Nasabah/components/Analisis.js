import React from 'react';
import '../Nasabah.scss';
import Pemasukan from './Pemasukan';
import Pengeluaran from './Pengeluaran';

const Analisis = () => {
    return (
        <div>
            <div className="catatan">
                <h6><span style={{ color: "#FF7715" }}>Catatan :</span> Data Analisis yang disajikan diperoleh dari transaksi rekening nasabah selama 3 (Tiga) bulan terakhir yakni Bulan <b>April, Mei, dan Juni</b></h6>
            </div>
            <div className="row">
                <div className="col-6">
                    <Pemasukan />
                </div>
                <div className="col-6">
                    <Pengeluaran />
                </div>
            </div>
        </div>
    )
}

export default Analisis
