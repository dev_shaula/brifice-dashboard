import React, { useState } from 'react';
import { TabContent, TabPane, NavLink, Row, Col } from 'reactstrap';
import classnames from 'classnames';
import './Nasabah.scss';
import Analisis from './components/Analisis';
import Portofolio from './components/Portofolio';

const NasabahDetail = () => {
    const [activeTab, setActiveTab] = useState('1');

    const toggle = tab => {
        if (activeTab !== tab) setActiveTab(tab);
    }
    return (
        <div>
            <h6 className="detail-nasabah">Detail Nasabah</h6>
            <ul className="nav nav-pills">
                <li className="nav-item" onClick={() => { toggle('1') }}>
                    <NavLink className={classnames({ active: activeTab === '1' })}>Data Diri</NavLink>
                </li>
                <li className="nav-item" onClick={() => { toggle('2') }}>
                    <NavLink className={classnames({ active: activeTab === '2' })}>Analisis</NavLink>
                </li>
                <li className="nav-item" onClick={() => { toggle('3'); }}>
                    <NavLink className={classnames({ active: activeTab === '3' })}>Portofolio</NavLink>
                </li>
            </ul>
            <TabContent activeTab={activeTab}>
                <TabPane tabId="1">
                    <Row>
                        <Col sm="12">
                            <label>Nama</label>
                            <h6>Giana Vaccaro</h6>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="12">
                            <label>Tanggal Lahir</label>
                            <h6>11 Januari 1990</h6>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="12">
                            <label>Email</label>
                            <h6>GinanaV@gmail.com</h6>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="12">
                            <label>Rekening BRI</label>
                            <h6>3323-02-012345-78-9</h6>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="12">
                            <label>Alamat</label>
                            <h6>Jl Syahrir, No 39, Dusun Cibetok RT02/RW09, Gambir, Jakarta Pusat. 10150</h6>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="12">
                            <label>Nomor HP</label>
                            <h6>0812-3456-7890</h6>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="12">
                            <label>Register</label>
                            <h6>12 Jul 2021 ; 12:40</h6>
                        </Col>
                    </Row>
                </TabPane>
                <TabPane tabId="2">
                    <Row>
                        <Col sm="12">
                            <Analisis />
                        </Col>
                    </Row>
                </TabPane>
                <TabPane tabId="3">
                    <Row>
                        <Col sm="12">
                            <Portofolio />
                        </Col>
                    </Row>
                </TabPane>
            </TabContent>
        </div>
    )
}

export default NasabahDetail
