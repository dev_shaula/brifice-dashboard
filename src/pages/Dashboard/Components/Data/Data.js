import React from 'react';
import propTypes from 'prop-types';
import './Data.scss';

const Data = ({ title, value, router, isActive }) => {
    return (
        <div className={`data ${isActive ? "active-data" : ""}`} onClick={router}>
            <p>{title}</p>
            <h1>{value}</h1>
        </div>
    )
}

export default Data

Data.propTypes = {
    title: propTypes.string,
    value: propTypes.number,
    router: propTypes.func,
    isActive: propTypes.bool
}