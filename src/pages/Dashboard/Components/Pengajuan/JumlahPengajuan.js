/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import JumlahPengajuanChart from './chart/JumlahPengajuanChart';
import { TabContent, TabPane, NavLink, Row, Col } from 'reactstrap';
import classnames from 'classnames';
import './Pengajuan.scss';
import { useDispatch, useSelector } from 'react-redux';
import { actionGetPengajuanByProduct, actionGetListProduct } from '../../../../store/actions';

const JumlahPengajuan = () => {
    const [activeTab, setActiveTab] = useState(1);
    const dispatch = useDispatch();
    const { product } = useSelector(state => state.info);
    const totalPengajuan = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    const [total, setTotal] = useState([])

    const toggle = tab => {
        if (activeTab !== tab) setActiveTab(tab);
        dispatch(actionGetPengajuanByProduct(tab))
    }

    useEffect(() => {
        dispatch(actionGetListProduct())
        if (activeTab === 1) {
            dispatch(actionGetPengajuanByProduct(1, graf => {
                for (let index = 0; index < graf.length; index++) {
                    totalPengajuan.splice(graf[index].bulan - 1, 1, graf[index].jumlah)
                    setTotal(totalPengajuan)
                }
            }))
        }
    }, [activeTab])

    return (
        <div>
            <ul className="nav nav-pills">
                {
                    product.map(val => (
                        <li key={val.id} className="nav-item" onClick={() => { toggle(val.id) }}>
                            <NavLink className={classnames({ active: activeTab === val.id })}>{val.nama_produk}</NavLink>
                        </li>
                    ))
                }
            </ul>
            <TabContent activeTab={activeTab}>
                {
                    product.map((val) => (
                        <TabPane tabId={val.id} key={val.id}>
                            <Row>
                                <Col sm="12">
                                    <JumlahPengajuanChart data={total} />
                                </Col>
                            </Row>
                        </TabPane>
                    ))
                }
            </TabContent>
        </div>
    )
}

export default JumlahPengajuan
