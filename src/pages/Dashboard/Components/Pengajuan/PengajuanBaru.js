import React from 'react';
import { Table } from '../../../../components';
import './Pengajuan.scss';
import PengajuanBaruRow from './table/PengajuanBaruRow';

const PengajuanBaru = (props) => {
    return (
        <div>
            <Table
                thead={[
                    "No",
                    "Time Stamp",
                    "Nama",
                    "Produk Investasi",
                    "Jumlah",
                    "Status"
                ]}
                data={props.data.pengajuan_baru}
                renderRow={(item, idx) => (
                    <PengajuanBaruRow
                        item={item}
                        key={idx}
                        idx={idx}
                    />
                )} 
            />
        </div>
    )
}

export default PengajuanBaru
