import React from 'react';
import propTypes from 'prop-types';
import moment from 'moment';
import '../Pengajuan.scss';
import { numberFormat } from '../../../../../utils';

const PengajuanBaruRow = ({ item, idx }) => {
    return (
        <tr key={idx + 1}>
            <td>{idx + 1}</td>
            <td>{moment(item.timestamp).format("DD MMM YYYY ; HH:MM")}</td>
            <td>{item.nama}</td>
            <td>{item.produk_investasi}</td>
            <td>Rp {numberFormat(item.nominal_pengajuan)}</td>
            <td>
                <div className={`status ${item.status === "submission" ? "status-subs" : item.status === "Forwarded" ? "status-forward" : item.status === "Approve" ? "status-approve" : "status-reject"}`}>
                    {item.status}
                </div>
            </td>
        </tr>
    )
}

export default PengajuanBaruRow

PengajuanBaruRow.propTypes = {
    item: propTypes.object
}