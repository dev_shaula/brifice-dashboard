/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import ReactApexChart from 'react-apexcharts';
import { useDispatch } from 'react-redux';
import { actionGetTotalPengajuan } from '../../../../../store/actions';
import './index.scss';

const TotalPengajuanChart = () => {
    const totalPengajuan = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    const [total, setTotal] = useState([])
    const dispatch = useDispatch();

    const options = {
        chart: {
            type: "area",
            height: 280,
        },
        xaxis: {
            categories: [
                "Januari",
                "Februari",
                "Maret",
                "April",
                "Mei",
                "Juni",
                "Juli",
                "Agustus",
                "September",
                "Oktober",
                "November",
                "Desember"
            ],
        },
        yaxis: {
            title: {
                text: undefined,
            },
            labels: {
                show: true,
                formatter: function (val) {
                    return val.toFixed(0)
                }
            },
            min: 10,
            tickAmount: 10
        },
        fill: {
            type: "gradient",
            gradient: {
                shadeIntensity: 1,
                opacityFrom: 0.7,
                opacityTo: 0.9,
                stops: [0, 90, 100]
            }
        },
        dataLabels: {
            enabled: false,
        },
        tooltip: {
            y: {
                formatter: function (value, { series, seriesIndex, dataPointIndex, w }) {
                    return value
                }
            }
        },
        noData: {
            text: "Loading...",
        }
    }

    useEffect(() => {
        dispatch(actionGetTotalPengajuan(graf => {
            for (let index = 0; index < graf.length; index++) {
                totalPengajuan.splice(graf[index].bulan - 1, 1, graf[index].jumlah)
                setTotal(totalPengajuan)
            }
        }))
    }, [])

    return (
        <div>
            <ReactApexChart options={options} series={[
                {
                    name: "Total Pengajuan",
                    // data: [45, 52, 38, 45, 19, 23, 2, 89, 65, 10, 54, 76]
                    data: total
                }
            ]} type="area" />
        </div>
    )
}

export default TotalPengajuanChart
