/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import ReactApexChart from 'react-apexcharts';
import { useDispatch } from 'react-redux';
import { actionTotalPengguna } from '../../../../../store/actions';
import './index.scss';

const TotalPenggunaChart = () => {
    const totalNasabah = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    const [total, setTotal] = useState([])
    const dispatch = useDispatch();

    const options = {
        chart: {
            type: "area",
            height: 280,
        },
        xaxis: {
            categories: [
                "Januari",
                "Februari",
                "Maret",
                "April",
                "Mei",
                "Juni",
                "Juli",
                "Agustus",
                "September",
                "Oktober",
                "November",
                "Desember"
            ],
        },
        yaxis: {
            title: {
                text: undefined,
            },
            labels: {
                show: true,
                formatter: function (val) {
                    return val.toFixed(0)
                }
            },
            min: 10,
            tickAmount: 10
        },
        fill: {
            type: "gradient",
            gradient: {
                shadeIntensity: 1,
                opacityFrom: 0.7,
                opacityTo: 0.9,
                stops: [0, 90, 100]
            }
        },
        dataLabels: {
            enabled: false,
        },
        tooltip: {
            y: {
                formatter: function (value, { series, seriesIndex, dataPointIndex, w }) {
                    return value
                }
            }
        },
        noData: {
            text: "Loading...",
        }
    }

    useEffect(() => {
        dispatch(actionTotalPengguna(graf => {
            for (let index = 0; index < graf.length; index++) {
                totalNasabah.splice(graf[index].bulan - 1, 1, graf[index].jumlah)
                setTotal(totalNasabah)
            }
        }))
    }, [])

    return (
        <div>
            <ReactApexChart options={options} series={[
                {
                    name: "Total Pengguna",
                    data: total
                }
            ]} type="area" />
        </div>
    )
}

export default TotalPenggunaChart
