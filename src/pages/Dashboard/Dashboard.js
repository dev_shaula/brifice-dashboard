import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { actionGetPengajuanBaru, actionGetDashboard } from '../../store/actions';
import { Data } from './Components';
import TotalPengajuan from './Components/Pengajuan/chart/TotalPengajuanChart';
import TotalPengguna from './Components/Pengajuan/chart/TotalPenggunaChart';
import JumlahPengajuan from './Components/Pengajuan/JumlahPengajuan';
import PengajuanBaru from './Components/Pengajuan/PengajuanBaru';
import './Dashboard.scss';

const Dashboard = () => {
    const [info, setInfo] = useState("pengajuan-baru");
    const { pengajuanBaru, dashboard } = useSelector(state => state.dashboard);

    const dispatch = useDispatch();
    const infoData = [
        {
            title: "Pengajuan Baru",
            value: dashboard.total_pengajuan_baru,
            router: "pengajuan-baru"
        },
        {
            title: "Jumlah Pengajuan",
            value: dashboard.total_pengajuan_produk,
            router: "jumlah-pengajuan"
        },
        {
            title: "Total Pengajuan",
            value: dashboard.total_pengajuan,
            router: "total-pengajuan"
        },
        {
            title: "Total Pengguna",
            value: dashboard.total_nasabah,
            router: "total-pengguna"
        },
    ]

    useEffect(() => {
        dispatch(actionGetDashboard())
        if (pengajuanBaru.pengajuan_baru.length <= 0) {
            dispatch(actionGetPengajuanBaru())
        }
    }, [dispatch, pengajuanBaru.pengajuan_baru])

    return (
        <div className="dashboard">
            <h6>Update!</h6>
            <div className="d-flex">
                {
                    infoData.map((data, idx) => (
                        <Data key={idx} title={data.title} value={data.value} router={() => setInfo(data.router)} isActive={info === data.router} />
                    ))
                }
            </div>
            {
                info === "pengajuan-baru" ? (
                    <section className="pengajuan-baru">
                        <h6>Pengajuan Baru</h6>
                        <PengajuanBaru data={pengajuanBaru} />
                    </section>
                ) : (
                    info === "jumlah-pengajuan" ? (
                        <section className="jumlah-pengajuan">
                            <h6>Jumlah Pengajuan</h6>
                            <JumlahPengajuan />
                        </section>
                    ) : (
                        info === "total-pengajuan" ? (
                            <section className="total-pengajuan">
                                <h6>Total Pengajuan</h6>
                                <TotalPengajuan />
                            </section>
                        ) : (
                            info === "total-pengguna" && (
                                <section className="total-pengguna">
                                    <h6>Total Pengguna</h6>
                                    <TotalPengguna />
                                </section>
                            )
                        )
                    )
                )
            }
        </div>
    )
}

export default Dashboard
