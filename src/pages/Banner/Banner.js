import React, { useState } from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import { Button, Modal, Table } from '../../components';
import { Pagination, SelectPicker } from 'rsuite';
import { FiSearch } from 'react-icons/fi';
import './Banner.scss';
import BannerRow from './BannerRow';
import { limit } from '../../utils';
import { ModalContent, Uploader } from './Components';

const Banner = () => {
    const [image, setImage] = useState('')
    console.log(image)
    const pengajuan = [
        {
            no: 1,
            update: "12 Jul 2021 ; 12:40",
            banner: "Promo Investasi",
            link: "https://prioritas.bri.co.id/site/product/...",
            status: "Active"
        },
        {
            no: 2,
            update: "12 Jul 2021 ; 12:40",
            banner: "Promo Investasi",
            link: "https://prioritas.bri.co.id/site/product/...",
            status: "Nonactive"
        },
        {
            no: 3,
            update: "12 Jul 2021 ; 12:40",
            banner: "Promo Investasi",
            link: "https://prioritas.bri.co.id/site/product/...",
            status: "Active"
        },
        {
            no: 4,
            update: "12 Jul 2021 ; 12:40",
            banner: "Promo Investasi",
            link: "https://prioritas.bri.co.id/site/product/...",
            status: "Nonactive"
        },
        {
            no: 5,
            update: "12 Jul 2021 ; 12:40",
            banner: "Promo Investasi",
            link: "https://prioritas.bri.co.id/site/product/...",
            status: "Active"
        }
    ]

    const [state, setState] = useState({
        prev: true,
        next: true,
        ellipsis: true,
        boundaryLinks: true,
        activePage: 1
    })

    const [open, setOpen] = useState(false)

    const handleSelect = (eventKey) => {
        setState({ activePage: eventKey });
    }

    return (
        <div className="banner">
            <h6>Upload Banner</h6>
            <div className="catatan">
                <h6><span style={{ color: "#FF7715" }}>Catatan :</span> Silakan upload gambar banner dengan ukuran 290 x 120, dengan format JPG, JPEG dan PNG,</h6>
            </div>
            <Form style={{ marginTop: "30px" }}>
                <Uploader
                    sendImage={(img) => setImage(img)}
                    maxSize={2}
                />
                <FormGroup>
                    <Label for="title" style={{ paddingBottom: "10px" }}>Tittle Banner</Label>
                    <Input style={{ paddingLeft: "10px" }} type="text" name="title" id="title" placeholder="Contoh : Promo BRIFICE" />
                </FormGroup>
                <FormGroup>
                    <Label for="deskripsi" style={{ paddingBottom: "10px" }}>Link Banner</Label>
                    <Input style={{ paddingLeft: "10px" }} type="text" name="deskripsi" id="deskripsi" placeholder="Contoh : promo.bri.co.id/product/..." />
                </FormGroup>
                <Button className="mt-2">
                    <h6 className="bold text-white m-0 p-0">SUBMIT</h6>
                </Button>
            </Form>
            <hr />
            <div className="list-banner">
                <h6>List Banner</h6>
                <div style={{ display: "flex", justifyContent: "space-between", marginBottom: "30px" }}>
                    <SelectPicker searchable={false} data={limit} style={{ width: 80 }} value={10} />
                    <div style={{ display: "flex", position: "relative" }}>
                        <div style={{ position: "absolute", top: "9px", left: "15px", paddingRight: "25px" }}>
                            <FiSearch size={18} color="#4A4A4A" />
                        </div>
                        <Input
                            type="text"
                            name="search"
                            placeholder="Pencarian..."
                            style={{
                                width: 350,
                                height: 40,
                                borderRadius: "10px",
                                boxShadow: "none",
                                paddingLeft: "50px"
                            }}
                        />
                    </div>
                </div>
                <Table
                    thead={[
                        "No",
                        "Update",
                        "Banner",
                        "Link",
                        "On/Off"
                    ]}
                    data={pengajuan}
                    renderRow={(item, key) => (
                        <BannerRow
                            item={item}
                            detailBanner={() => setOpen(true)}
                            key={key}
                        />
                    )}
                />
                <div style={{ display: "flex", justifyContent: "space-between", marginTop: "40px" }}>
                    <Pagination
                        prev
                        last
                        next
                        first
                        pages={30}
                        maxButtons={5}
                        activePage={state.activePage}
                        onSelect={handleSelect}
                        size="sm"
                    />
                    <span style={{ color: "#898A8D" }}>Showing <b>1 - 10</b> of 1000 Items</span>
                </div>
            </div>
            {
                open && (
                    <Modal onClose={() => setOpen(false)} body={<ModalContent />} />
                )
            }
        </div>
    )
}

export default Banner
