import React, { useRef, useState } from 'react';
import { IconClose, IconUploadBanner } from '../../../assets';
import '../Banner.scss';

const Uploader = (props) => {
    let uploadRef = useRef()
    const [image, setImage] = useState(props.getImage)

    const readFile = (file) => {
        return new Promise(res => {
            const reader = new FileReader()
            reader.addEventListener('load', () => {
                res(reader.result)
            })
            reader.readAsDataURL(file)
        })
    }

    const selectFile = async (e) => {
        const allowType = ["jpg", "jpeg", "png"]
        const maxSize = 2000000;
        const { name: fileName, size: fileSize } = e.target.files[0]
        const fileType = fileName.split(".").pop()
        if (!allowType.includes(fileType)) {
            console.log("warning", "File type not allowed")
        } else if (fileSize > maxSize) {
            if (props.maxSize === 2) {
                console.log("warning", "Gambar banner terlalu besar, maksimal 2 MB")
            }
        } else {
            if (e.target.files && e.target.files.length > 0) {
                const file = e.target.files[0]
                let image = await readFile(file)
                const imgElement = document.createElement("img")
                imgElement.src = image

                imgElement.onload = function (e) {
                    const canvas = document.createElement("canvas")
                    const MAX_WIDTH = 400

                    const scaleSize = MAX_WIDTH / e.target.width
                    canvas.width = MAX_WIDTH
                    canvas.height = e.target.height * scaleSize

                    const ctx = canvas.getContext("2d")
                    ctx.drawImage(e.target, 0, 0, canvas.width, canvas.height)

                    const srcEncoded = ctx.canvas.toDataURL(e.target, "image/jpeg", 2.0)

                    setImage(srcEncoded)
                    props.sendImage(srcEncoded)
                }
                // setImage(image)
                // props.sendImage(image)
            }
        }
    }

    return (
        <div>
            <div className={`upload-border-dashed ${image && "upload-preview"}`} onClick={() => uploadRef.click()}>
                <div>
                    <input
                        type="file"
                        id="file"
                        ref={input => uploadRef = input}
                        className="form-control-file absolute"
                        name={props.name}
                        onChange={selectFile}
                        disabled={image}
                    />
                    {
                        image || props.getImage ? (
                            <div>
                                <img className="upload-banner-preview absolute top-0" src={props.getImage ? props.getImage : image} alt="" />
                                <div className="close-modal" onClick={() => setImage("")}>
                                    <img src={IconClose} alt="" />
                                </div>
                            </div>
                        ) : (
                            <div className="upload-banner-text">
                                <img src={IconUploadBanner} alt="" />
                                <span className="text-center">
                                    Upload
                                </span>
                            </div>
                        )
                    }
                </div>
            </div>
        </div>
    )
}

export default Uploader
