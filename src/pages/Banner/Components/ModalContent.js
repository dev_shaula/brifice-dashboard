import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import { ExampleBanner } from '../../../assets';
import '../Banner.scss';

const ModalContent = () => {
    return (
        <div>
            <div className="image-container">
                <img src={ExampleBanner} className="image-banner" alt="banner" />
            </div>
            <Form style={{ marginTop: "30px" }}>
                <FormGroup>
                    <Label for="title" style={{ paddingBottom: "10px" }}>Tittle Banner</Label>
                    <Input style={{ paddingLeft: "10px" }} type="text" name="title" id="title" value="Promo Investasi Pertama" disabled />
                </FormGroup>
                <FormGroup>
                    <Label for="deskripsi" style={{ paddingBottom: "10px" }}>Link Banner</Label>
                    <Input style={{ paddingLeft: "10px" }} type="text" name="deskripsi" id="deskripsi" value="https://campaign.bri.co.id/site/product/Davestera-20210721-NewUser.html" disabled />
                </FormGroup>
            </Form>
        </div>
    )
}

export default ModalContent
