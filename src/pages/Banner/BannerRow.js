import React from 'react';
import propTypes from 'prop-types';
import { Toggle } from 'rsuite';

const BannerRow = ({ item, detailBanner }) => {
    return (
        <tr key={item.no}>
            <td onClick={detailBanner}>{item.no}</td>
            <td onClick={detailBanner}>{item.update}</td>
            <td onClick={detailBanner}>{item.banner}</td>
            <td onClick={detailBanner}>
                <a href={item.link}>{item.link}</a>
            </td>
            <td>
                <Toggle checked={item.status === "Active" ? true : false} />
            </td>
        </tr>
    )
}

export default BannerRow

BannerRow.propTypes = {
    item: propTypes.object
}