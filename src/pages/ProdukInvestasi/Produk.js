import React, { useState } from 'react';
import { Form, FormGroup, Label, Input, Row, Col } from 'reactstrap';
import { Button } from '../../components';
import { Pagination, Checkbox, SelectPicker } from 'rsuite';
import { FiSearch } from 'react-icons/fi';
import { Table } from '../../components';
import ProdukRow from './ProdukRow';
import './Produk.scss';
import { limit } from '../../utils';

const Produk = () => {
    const pengajuan = [
        {
            no: 1,
            registed: "12 Jul 2021 ; 12:40",
            nama: "Danareksa Seruni Pasar Uang II",
            jenis: "Reksadana Pasar Uang",
            risiko: "Rendah"
        },
        {
            no: 2,
            registed: "12 Jul 2021 ; 12:40",
            nama: "Danareksa Seruni Pasar Uang II",
            jenis: "Reksadana Pasar Uang",
            risiko: "Rendah"
        },
        {
            no: 3,
            registed: "12 Jul 2021 ; 12:40",
            nama: "Danareksa Seruni Pasar Uang II",
            jenis: "Reksadana Pasar Uang",
            risiko: "Rendah"
        },
        {
            no: 4,
            registed: "12 Jul 2021 ; 12:40",
            nama: "Danareksa Seruni Pasar Uang II",
            jenis: "Reksadana Pasar Uang",
            risiko: "Rendah"
        },
        {
            no: 5,
            registed: "12 Jul 2021 ; 12:40",
            nama: "Danareksa Seruni Pasar Uang II",
            jenis: "Reksadana Pasar Uang",
            risiko: "Rendah"
        }
    ]

    const [state, setState] = useState({
        prev: true,
        next: true,
        ellipsis: true,
        boundaryLinks: true,
        activePage: 1
    })

    const handleSelect = (eventKey) => {
        setState({ activePage: eventKey });
    }

    return (
        <div className="produk-invest">
            <h6>Produk Investasi</h6>
            <Form style={{ marginTop: "30px" }}>
                <FormGroup>
                    <Label for="nama" style={{ paddingBottom: "10px" }}>Nama Produk Investasi</Label>
                    <Input style={{ paddingLeft: "10px" }} type="text" name="nama" id="nama" placeholder="Contoh : Pemeliharaan Sistem" />
                </FormGroup>
                <FormGroup>
                    <Label for="deskripsi" style={{ paddingBottom: "10px" }}>Deskripsi</Label>
                    <Input style={{ paddingLeft: "10px", boxShadow: "none" }} type="textarea" name="deskripsi" id="deskripsi" placeholder="Contoh : Akan ada pemeliharaan sistem hari ini pukul 21:00 - 23:59 WIB" />
                </FormGroup>
            </Form>
            <div style={{ marginTop: "30px" }}>
                <p><b>Jenis Produk</b></p>
                <Row>
                    <Col sm="6">
                        <Checkbox>Reksadana Pasar Uang</Checkbox>
                        <Checkbox>SBN</Checkbox>
                        <Checkbox>Reksadana Pendapatan Tetap</Checkbox>
                    </Col>
                    <Col sm="6">
                        <Checkbox>Reksadana Pendapatan Tetap</Checkbox>
                        <Checkbox>Reksadana Pendapatan Tetap</Checkbox>
                    </Col>
                </Row>
                <p style={{ marginTop: "30px" }}><b>Level Risiko</b></p>
                <Row>
                    <Col sm="12">
                        <Checkbox>Rendah/ Konservatif</Checkbox>
                        <Checkbox>Moderate</Checkbox>
                        <Checkbox>Agresive</Checkbox>
                    </Col>
                </Row>
                <Button className="mt-4">
                    <h6 className="bold text-white">Tambah</h6>
                </Button>
            </div>
            <hr />
            <div className="list-invest">
                <h6>List Produk Investasi</h6>
                <div style={{ display: "flex", justifyContent: "space-between", marginBottom: "30px" }}>
                    <SelectPicker searchable={false} data={limit} style={{ width: 80 }} value={10} />
                    <div style={{ display: "flex", position: "relative" }}>
                        <div style={{ position: "absolute", top: "9px", left: "15px", paddingRight: "25px" }}>
                            <FiSearch size={18} color="#4A4A4A" />
                        </div>
                        <Input
                            type="text"
                            name="search"
                            placeholder="Pencarian..."
                            style={{
                                width: 350,
                                height: 40,
                                borderRadius: "10px",
                                boxShadow: "none",
                                paddingLeft: "50px"
                            }}
                        />
                    </div>
                </div>
                <Table
                    thead={[
                        "No",
                        "Registed",
                        "Nama Produk",
                        "Jenis Produk",
                        "Risiko"
                    ]}
                    data={pengajuan}
                    renderRow={(item, key) => (
                        <ProdukRow
                            item={item}
                            key={key}
                        />
                    )}
                />
                <div style={{ display: "flex", justifyContent: "space-between", marginTop: "40px" }}>
                    <Pagination
                        prev
                        last
                        next
                        first
                        pages={30}
                        maxButtons={5}
                        activePage={state.activePage}
                        onSelect={handleSelect}
                        size="sm"
                    />
                    <span style={{ color: "#898A8D" }}>Showing <b>1 - 10</b> of 1000 Items</span>
                </div>
            </div>
        </div>
    )
}

export default Produk
