import React from 'react';
import propTypes from 'prop-types';

const ProdukRow = ({ item }) => {
    return (
        <tr key={item.no}>
            <td>{item.no}</td>
            <td>{item.registed}</td>
            <td>{item.nama}</td>
            <td>{item.jenis}</td>
            <td>{item.risiko}</td>
        </tr>
    )
}

export default ProdukRow

ProdukRow.propTypes = {
    item: propTypes.object
}