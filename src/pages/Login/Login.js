/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState } from 'react';
import { Input, Button } from "../../components";
import './Login.scss';
import { Link, useHistory } from 'react-router-dom';
import { actionLogin, isAuthenticated } from '../../store/actions';
import { IconLogo, LoginBg } from '../../assets';
import { useSelector, useDispatch } from 'react-redux';
import { Message } from 'rsuite';

const Login = () => {
    const [form, setForm] = useState({
        username: "",
        password: ""
    });
    const history = useHistory()
    const { loading } = useSelector(state => state.auth)
    const dispatch = useDispatch()

    const onChange = e => {
        setForm({ ...form, [e.target.name]: e.target.value })
    }

    const signIn = () => {
        dispatch(actionLogin(form, (status, message) => {
            if (status === true) {
                return history.push("/dashboard")
            }
        }))
    }

    const ReturnToDashboard = () => {
        return (
            <div>
                <p>Anda sudah login, silahkan pergi ke <Link to="/dashboard">Dashboard</Link></p>
            </div>
        )
    }

    return (
        <div className="section-logo">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-4">
                        <section className="login">
                            <div className="logo">
                                <img src={IconLogo} alt="" />
                            </div>
                            <div className="form-login">
                                <h1>Selamat Datang</h1>
                                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Reiciendis repellendus nostrum fugiat sint distinctio aspernatur magni asperiores vel dolorum facere?</p>
                                <Input
                                    name="username"
                                    isPassword={false}
                                    placeholder="Username"
                                    onChange={onChange}
                                    value={form.username}
                                />
                                <Input
                                    name="password"
                                    isPassword
                                    placeholder="Password"
                                    onChange={onChange}
                                    value={form.password}
                                />
                                <Button className="mt-2" onClick={signIn}>
                                    {
                                        !loading ? (
                                            <h5 className="text-white fw-bolder">MASUK</h5>
                                        ) : (
                                            <div style={{ display: "flex", justifyContent: "center" }}>
                                                <div className="spinner-border spinner-border-sm text-light" role="status">
                                                    <span className="visually-hidden">Loading...</span>
                                                </div>
                                            </div>
                                        )
                                    }
                                </Button>
                            </div>
                            <p className="copyright text-center">Copyright @ BRI 2021</p>
                        </section>
                    </div>
                    <div className="col-8">
                        <img src={LoginBg} className="background" alt="" />
                    </div>
                </div>
            </div>
            {
                isAuthenticated() && (
                    <Message full showIcon description={<ReturnToDashboard />} />
                )
            }
        </div>
    )
}

export default Login
